﻿#include <iostream>
#include <string>
#include <time.h>


int main()
{
    const int N{ 5 };
    int array[N][N];

    // filling and printing the array
    for (int i = 0; i < N; ++i) {
        std::cout << "[ ";

        for (int j = 0; j < N; ++j) {
            int el{ i + j };

            array[i][j] = el;
            std::cout << el << ' ';
        }

        std::cout << "]\n";
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    int day{ buf.tm_mday };
    // индекс искомой строки
    int rowIndex = day % N;

    int sumOfDays{ 0 };
    for (int i = 0; i < N; ++i) {
        sumOfDays += array[rowIndex][i];
    }
    std::cout << sumOfDays << '\n';
}
